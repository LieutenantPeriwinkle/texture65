#include "tex_viewer.h"
#include "macros.h"

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#include "nuklear.h"

enum nk_widget_layout_states t65_tex_viewer_scrolled(struct nk_context *ctx, t65_tex_info *info) {
    struct nk_command_buffer *buf = nk_window_get_canvas(ctx);

    struct nk_rect total_space;
    enum nk_widget_layout_states state = nk_widget(&total_space, ctx);

    if (state != NK_WIDGET_INVALID) {
        total_space = nk_rect(total_space.x - 1, total_space.y - 1, total_space.w + 2, total_space.h + 2);
        nk_stroke_rect(buf, total_space, 0, 1, nk_rgb(0, 0, 0));
        total_space = nk_rect(total_space.x + 1, total_space.y + 1, total_space.w - 2, info->location.h);
        nk_draw_image(buf, total_space, &info->texture, nk_rgb(255, 255, 255));
        nk_layout_space_push(ctx, nk_rect(total_space.x - 6, total_space.y - 50, CLAMP_MIN(total_space.w, 35), 20));
        nk_text(ctx, info->title, strlen(info->title), NK_TEXT_ALIGN_LEFT);
    }
    return state;
}

void t65_tex_viewer(struct nk_context *ctx, t65_tex_info *info) {
    struct nk_command_buffer *buf = nk_window_get_canvas(ctx);

    struct nk_rect total_space = nk_rect(0, 0, info->location.w, info->location.h);
    enum nk_widget_layout_states state = nk_widget(&total_space, ctx);

    if (state != NK_WIDGET_INVALID) {
        nk_draw_image(buf, total_space, &info->texture, nk_rgb(255, 255, 255));
        total_space = nk_rect(total_space.x - 1, total_space.y - 1, total_space.w + 2, total_space.h + 2);
        nk_stroke_rect(buf, total_space, 0, 1, nk_rgb(0, 0, 0));
        nk_layout_space_push(ctx, nk_rect(total_space.x - 6, total_space.y - 50, CLAMP_MIN(total_space.w, 35), 20));
        nk_text(ctx, info->title, strlen(info->title), NK_TEXT_ALIGN_LEFT);
    }
}

void t65_tex_viewer_custom(struct nk_context *ctx, t65_tex_info *info, float enc_ratio, size_t base_addr) {
    struct nk_command_buffer *buf = nk_window_get_canvas(ctx);

    struct nk_rect total_space = nk_rect(0, 0, info->location.w, info->location.h);
    enum nk_widget_layout_states state = nk_widget(&total_space, ctx);

    if (state != NK_WIDGET_INVALID) {
        nk_draw_image(buf, total_space, &info->texture, nk_rgb(255, 255, 255));
        total_space = nk_rect(total_space.x - 1, total_space.y - 1, total_space.w + 2, total_space.h + 2);
        nk_stroke_rect(buf, total_space, 0, 1, nk_rgb(0, 0, 0));
        nk_layout_space_push(ctx, nk_rect(total_space.x - 6, total_space.y - 50, CLAMP_MIN(total_space.w, 35), 20));
        nk_text(ctx, info->title, strlen(info->title), NK_TEXT_ALIGN_LEFT);
        size_t addr = enc_ratio * info->uv.x * info->uv.y + base_addr;
        char end_address[24] = { 0 };
        snprintf(end_address, 20, "next: 0x%zx", addr);
        nk_layout_space_push(ctx, nk_rect(total_space.x - 6, total_space.y + total_space.h - 30, total_space.w, 20));
        nk_text(ctx, end_address, strlen(end_address), NK_TEXT_ALIGN_LEFT);
    }
}
