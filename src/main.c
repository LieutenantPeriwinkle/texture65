#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#include "nuklear.h"
#include "nuklear_sdl_gl3.h"

#include "macros.h"
#include "img_enc.h"
#include "tex_viewer.h"
#include "ui.h"

int main(int argc, char **argv) {
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTS);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    SDL_Window *win = SDL_CreateWindow(WIN_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIN_WIDTH, WIN_HEIGHT,
                                       SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    SDL_GLContext gl_ctx = SDL_GL_CreateContext(win);

    glViewport(0, 0, WIN_WIDTH, WIN_HEIGHT);
    glewExperimental = 1;
    if (glewInit() != GLEW_OK) {
        fputs("Failed to initialize GLEW!", stderr);
        exit(1);
    }

    struct nk_context *ctx = nk_sdl_init(win);
    if (ctx == NULL) {
        fputs("Failed to initialize Nuklear!", stderr);
        exit(1);
    }
    t65_font_setup(ctx);

    SDL_Event ev;
    int32_t w = WIN_WIDTH, h = WIN_HEIGHT;
    bool run = true;

    t65_tex_info infos[] = {
        { .location = nk_rect(2, 45, 150, SCROLL_VIEWER_HEIGHT), .uv = nk_vec2(75, 50), .px_size = 1, .title = "File" },
        // top row
        { .location = nk_rect(170, 45, 192, 192), .uv = nk_vec2(64, 64), .title = "64x64" },
        { .location = nk_rect(380, 45, 96, 192), .uv = nk_vec2(32, 64), .title = "32x64" },
        // middle row
        { .location = nk_rect(170, 259, 96, 96), .uv = nk_vec2(32, 32), .title = "32x32" },
        { .location = nk_rect(284, 259, 48, 96), .uv = nk_vec2(16, 32), .title = "16x32" },
        // bottom row
        { .location = nk_rect(170, 377, 48, 48), .uv = nk_vec2(16, 16), .title = "16x16" },
        { .location = nk_rect(236, 377, 24, 48), .uv = nk_vec2(8, 16), .title = "8x16" },
        { .location = nk_rect(278, 377, 24, 24), .uv = nk_vec2(8, 8), .title = "8x8" },
        // custom
        { .location = nk_rect(380, 259, 192, 192), .uv = nk_vec2(64, 64), .title = "Custom" },
    };
    GLuint textures[ARRLEN(infos)];
    glGenTextures(ARRLEN(textures), textures);
    for (size_t i = 0; i < ARRLEN(textures); i++) {
        infos[i].texture = nk_image_id(textures[i]);
    }

    char *filename = argc > 1 ? argv[1] : NULL;
    bool first_run = filename != NULL;

    const char *scales[] = { "1", "2", "3" };

    while (run) {
        nk_input_begin(ctx);
        while (SDL_PollEvent(&ev)) {
            if (ev.type == SDL_QUIT)
                run = false;
            nk_sdl_handle_event(&ev);
        }
        nk_input_end(ctx);

        if (nk_begin(ctx, WIN_TITLE, nk_rect(0, 0, WIN_WIDTH, WIN_HEIGHT), NK_WINDOW_BORDER | NK_WINDOW_TITLE)) {
            static t65_img_encoding selected_enc = RGBA16;
            static t65_img_encoding last_enc = RGBA16;
            static t65_img_alpha selected_alpha = INTENSITY;
            static t65_img_alpha last_alpha = INTENSITY;

            static char tlut_addr_raw[64];
            static size_t tlut_addr = 0;
            static size_t last_tlut_addr = 0;

            static bool tlut = false;
            static bool alpha = false;
            static bool update = false;
            static bool update_scroll = false;
            static bool update_size = false;

            static size_t scroll = 0;
            static uint8_t *raw = NULL;
            static size_t raw_len = 0;
            static size_t data_len = 0;
            static struct nk_color *data = NULL;
            static float enc_ratio = 0.0;

            static char base_addr_raw[64] = { '0' };
            static size_t base_addr = 0;
            static size_t input_base_addr = 0;
            static uint8_t scale = 2;

            nk_layout_space_begin(ctx, NK_STATIC, 25, INT_MAX);

            nk_layout_space_push(ctx, nk_rect(0, 0, 60, 25));
            if (nk_button_label(ctx, "Open")) {
                char *temp = t65_file_prompt();
                if (temp != NULL) {
                    filename = temp;
                    if (raw != NULL) {
                        free(raw);
                    }
                    raw_len = t65_file_read(filename, &raw);
                    if (raw_len != 0) {
                        update = update_scroll = true;
                    }
                }
            } else if (first_run) {
                raw_len = t65_file_read(filename, &raw);
                first_run = false;
                if (raw_len != 0) {
                    update = update_scroll = true;
                }
            }

            nk_layout_space_push(ctx, nk_rect(250, 0, 60, 25));
            nk_label(ctx, "Scale:", NK_TEXT_RIGHT);
            nk_layout_space_push(ctx, nk_rect(315, 0, 60, 25));
            uint8_t new = nk_combo(ctx, scales, 3, scale, 25, nk_vec2(50, 95));
            if (new != scale) {
                scale = new;
                for (size_t i = 1; i < ARRLEN(infos); i++) {
                    infos[i].location.w = infos[i].uv.x * (scale + 1);
                    infos[i].location.h = infos[i].uv.y * (scale + 1);
                }
            }

            if (filename != NULL) {
                input_base_addr = base_addr;
                if (base_addr != 0 || update) {
                    memset(base_addr_raw, 0, ARRLEN(base_addr_raw));
                    snprintf(base_addr_raw, ARRLEN(base_addr_raw), "%zx", base_addr);
                }
                nk_flags flags = t65_base_addr_edit(ctx, base_addr_raw);
                input_base_addr = strtoull(base_addr_raw, NULL, 16);
                t65_base_addr_increment(ctx, &input_base_addr, flags);
                if (input_base_addr != base_addr) {
                    snprintf(base_addr_raw, ARRLEN(base_addr_raw), "%zx", input_base_addr);
                    if (input_base_addr < raw_len) {
                        update = update_size = true;
                        base_addr = input_base_addr;
                    }
                }
                static int width = 64;
                static int height = 64;
                nk_layout_space_push(ctx, nk_rect(250, 459, 115, 25));
                width = nk_propertyi(ctx, "Width:", 8, width, 128, 1, 1);
                nk_layout_space_push(ctx, nk_rect(250, 488, 115, 25));
                height = nk_propertyi(ctx, "Height:", 8, height, 128, 1, 1);
                if (width != infos[ARRLEN(infos) - 1].uv.x || height != infos[ARRLEN(infos) - 1].uv.y) {
                    infos[ARRLEN(infos) - 1].uv = nk_vec2(width, height);
                    t65_tex_info *custom_info = &infos[ARRLEN(infos) - 1];
                    t65_gen_texture(textures[ARRLEN(textures) - 1], data, custom_info->uv);
                    custom_info->location.w = custom_info->uv.x * (scale + 1);
                    custom_info->location.h = custom_info->uv.y * (scale + 1);
                }
            }

            selected_enc = t65_enc_select(ctx, selected_enc, w);
            if (selected_enc != last_enc) {
                last_enc = selected_enc;
                if (selected_enc == CI4 || selected_enc == CI8) {
                    tlut = true;
                    alpha = false;
                } else if (selected_enc == I4 || selected_enc == I8) {
                    alpha = true;
                    tlut = false;
                } else {
                    alpha = false;
                    tlut = false;
                }
                update = update_scroll = true;
            }

            if (tlut) {
                tlut_addr = t65_tlut_addr_edit(ctx, tlut_addr_raw, w);
                if (tlut_addr != last_tlut_addr) {
                    update = true;
                    last_tlut_addr = tlut_addr;
                }
            } else if (alpha) {
                selected_alpha = t65_alpha_select(ctx, selected_alpha, w);
                if (selected_alpha != last_alpha) {
                    last_alpha = selected_alpha;
                    update = true;
                }
            }

            if (update) {
                free(data);
                if (update_scroll) {
                    base_addr = 0;
                    enc_ratio = t65_enc_ratio_get(selected_enc);
                    scroll = 0;
                }

                data = t65_img_decode(&raw[base_addr], raw_len - base_addr, &raw[tlut_addr], selected_enc,
                                      selected_alpha, &data_len);

                if (update_size || update_scroll) {
                    infos[0].uv.y =
                        ceil(CLAMP_MAX((float)SCROLL_VIEWER_HEIGHT / infos[0].px_size, data_len / infos[0].uv.x));
                    infos[0].location.h =
                        ceil(CLAMP_MAX(SCROLL_VIEWER_HEIGHT, data_len / infos[0].uv.x * infos[0].px_size));
                }

                for (size_t i = 0; i < ARRLEN(textures); i++) {
                    t65_gen_texture(textures[i], data, infos[i].uv);
                }

                update = update_scroll = update_size = false;
            }

            if (filename != NULL) {
                nk_layout_space_push(ctx, nk_rect_h(infos[0].location, SCROLL_VIEWER_HEIGHT));
                if (t65_tex_viewer_scrolled(ctx, &infos[0]) != NK_WIDGET_INVALID) {
                    struct nk_input *in = &ctx->input;
                    int32_t dy = -in->mouse.scroll_delta.y;
                    size_t total_v = ((raw_len / enc_ratio) / infos[0].uv.x);
                    size_t vy = abs(dy);

                    if (dy < 0 && vy > scroll) {
                        if (scroll != 0) {
                            scroll = 0;
                            base_addr = 0;
                            update = update_size = true;
                        }
                    } else if ((dy > 0 && vy + scroll < total_v) || (dy < 0 && scroll >= vy)) {
                        base_addr += dy * infos[0].uv.x * enc_ratio;
                        scroll += dy;
                        update = update_size = true;
                    } else if (dy > 0 && vy + scroll >= total_v) {
                        if (scroll != total_v) {
                            update = update_size = true;
                            scroll = total_v;
                            base_addr = scroll * infos[0].uv.x * enc_ratio;
                        }
                    }
                }

                for (size_t i = 1; i < ARRLEN(infos) - 1; i++) {
                    nk_layout_space_push(ctx, infos[i].location);
                    t65_tex_viewer(ctx, &infos[i]);
                }
                nk_layout_space_push(ctx, infos[ARRLEN(infos) - 1].location);
                t65_tex_viewer_custom(ctx, &infos[ARRLEN(infos) - 1], enc_ratio, base_addr);
            }

            nk_layout_space_end(ctx);
            nk_end(ctx);
        }

        SDL_GetWindowSize(win, &w, &h);
        glViewport(0, 0, w, h);
        glClear(GL_COLOR_BUFFER_BIT);
        glClearColor(0, 0, 0, 0);
        nk_sdl_render(NK_ANTI_ALIASING_ON, MAX_VERTEX_MEMORY, MAX_ELEMENT_MEMORY);
        SDL_GL_SwapWindow(win);
    }

    SDL_GL_DeleteContext(gl_ctx);
    SDL_DestroyWindow(win);
    SDL_Quit();
    return 0;
}
