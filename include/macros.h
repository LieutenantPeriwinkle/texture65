#ifndef MACROS_H_
#define MACROS_H_

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define WIN_TITLE "texture65"
#define SCROLL_VIEWER_HEIGHT 500
#define MAX_VERTEX_MEMORY 512 * 1024
#define MAX_ELEMENT_MEMORY 128 * 1024

#define CLAMP_MAX(x, max) ((x) > (max) ? (max) : (x))
#define CLAMP_MIN(x, min) ((x) < (min) ? (min) : (x))
#define ARRLEN(arr) (sizeof(arr) / sizeof(arr[0]))
#define nk_rect_h(src, h) nk_rect(src.x, src.y, src.w, h)

#endif // MACROS_H_
