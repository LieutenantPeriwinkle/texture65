#ifndef IMG_ENC_H_
#define IMG_ENC_H_

#include <stddef.h>
#include <stdint.h>
#include <GL/glew.h>
#include <GL/gl.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#include "nuklear.h"

typedef enum { OBPP, I4, IA4, CI4, I8, IA8, CI8, IA16, RGBA16, RGBA32 } t65_img_encoding;

typedef enum { INTENSITY, BINARY, FULL } t65_img_alpha;

extern const char *encs[10];
extern const char *alphas[3];

struct nk_color *t65_img_decode(uint8_t *data, size_t data_len, uint8_t *tlut, t65_img_encoding enc,
                                t65_img_alpha alpha, size_t *out_len);
void t65_gen_texture(GLuint texture, struct nk_color *data, struct nk_vec2 uv);
float t65_enc_ratio_get(t65_img_encoding enc);

#endif // IMG_ENC_H_
